class SearchController < ApplicationController
  def index
    @result = OMDBApi.search params[:query]
    render json: @result
  end
end
