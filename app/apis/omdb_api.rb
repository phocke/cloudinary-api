require "redis"

class OMDBApi
  class << self
    def search query
      fetch_result_json URI.escape query
    end

private

    def omdb_client
      #TODO we should handle it gracefully too many results gracefully => "{"Response":"False","Error":"Too many results."}"
      @@omdb_client ||= Faraday.new(
        url: 'http://www.omdbapi.com/',
        params: {apikey: 'a1211eb4'}, #TODO should go to .env
        headers: {'Content-Type' => 'application/json'}
      )
    end

    def redis_client
      @@redis ||= Redis.new
    end

    def fetch_result_json query
      key = "OMDBApi:#{query}"
      redis_client.del(key) if Rails.env.development? # To prevent caching

      if result = redis_client.get(key)
        result
      else
        json = omdb_client.get do |req|
          req.params['s'] = query
        end.body

        redis_client.set(key, json)
        redis_client.get(key)
      end
    end
  end
end
